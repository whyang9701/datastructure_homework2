#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int cityNumber = 100;

char *startCity;
char *endCity;
char *cargoWeight;

const int MAX_CITY_NUM = 1000;
struct City
{
    char *name;

    int throughput;
};
struct City *cities; //所有城市
int **tunnelMap;     // tunnelMap[1,2] = 1 表示 1到2的路徑相通

struct OUTPUT
{
    char *data;
    struct OUTPUT *next;
};

void printOUTPUT(struct OUTPUT *output)
{
    if (output->next == NULL)
    { //這題沒有答案
        printf("NULL \n");
        return;
    }
    struct OUTPUT *now = output->next;
    while (1)
    {
        printf("%s\n", now->data);
        if (now->next == NULL)
        {
            break;
        }
        now = now->next;
    }
}

struct OUTPUT *addOUTPUT(struct OUTPUT *output, char *data)
{

    struct OUTPUT *now = output;
    int result = strcmp(now->data, data);

    //如果只有一筆資料
    if (now->next == NULL)

    {

        struct OUTPUT *o = malloc(sizeof(struct OUTPUT));
        o->data = data;
        //放後面或是前面
        if (result <= 0)
        {
            o->next = NULL;
            now->next = o;
            return now;
        }
        else
        {

            o->next = now;
            now->next = NULL;
            return o;
        }
    }
    //排在比第一筆資料更前面
    if (result >= 0)
    {
        struct OUTPUT *o = malloc(sizeof(struct OUTPUT));
        o->data = data;
        o->next = output;

        return o;
    }
    int result2 = 0;
    // 排在中間或是尾巴
    while (1)
    {
        result = strcmp(now->data, data);
        result2 = strcmp(data, now->next->data);
        // printf("result: %d result2: %d, %s %s %s \n", result, result2, now->data, data, now->next->data);
        if (result <= 0 && result2 <= 0)
        {
            struct OUTPUT *o = malloc(sizeof(struct OUTPUT));
            struct OUTPUT *temp = now->next;
            now->next = o;
            o->next = temp;
            o->data = data;
            //printOUTPUT(output);
            break;
        }

        now = now->next;
        if (now->next == NULL)
        {
            struct OUTPUT *o = malloc(sizeof(struct OUTPUT));
            o->data = data;
            o->next = NULL;
            now->next = o;
            break;
        }
    }
    return output;
}

struct PATH
{
    char *city;
    struct PATH *next;
};

struct PATH *addPathPoint(struct PATH *start, char *city)
{
    struct PATH *now = start;
    struct PATH *copy = malloc(sizeof(struct PATH));
    struct PATH *copyNow = copy;

    while (1)
    {
        if (now->next == NULL)
        {
            //printf("no more route\n");
            copyNow->city = now->city;

            copyNow->next = malloc(sizeof(struct PATH));
            copyNow->next->city = city;
            copyNow->next->next = NULL;
            //printf("copyNow->city:%s copyNow->next->city:%s\n", copyNow->city,copyNow->next->city);

            break;
        }
        copyNow->city = now->city;
        copyNow->next = malloc(sizeof(struct PATH));
        copyNow = copyNow->next;
        now = now->next;
    }
    return copy;
}

char *getPathOUTPUT(struct PATH *start)
{
    struct PATH *now = start->next;
    char *output = now->city;
    now = now->next;
    while (1)
    {
        char *copy = malloc(sizeof(char *));
        char *copy2 = malloc(sizeof(char *));
        strcpy(copy, output);
        sprintf(copy2, "%s,%s", copy, now->city);
        output = copy2;
        if (now->next == NULL)
        {
            break;
        }
        now = now->next;
    }
    return output;
}
struct PATH *getNewPath()
{
    struct PATH *path1 = malloc(sizeof(struct PATH));
    path1->city = "";
    path1->next = NULL;
    return path1;
}

struct PATH *getEndOfPath(struct PATH *path1)
{
    struct PATH *now = path1;
    while (now->next != NULL)
    {
        now = now->next;
    }
    return now;
}

int getCityNo(char *name)
{
    int cityNo = 0;
    for (int i = 0; i < cityNumber; i++)
    {
        if (strcmp(name, cities[i].name) == 0)
        {
            cityNo = i;
        }
    }
    return cityNo;
}
int isCityInTheCity(struct PATH *path, char *city)
{
    struct PATH *now = path;
    while (now->next != NULL)
    {
        if (strcmp(now->city, city) == 0)
        {
            return 1; //這個城市已經出現在路經中
        }
        now = now->next;
    }
    return 0;
}
int isCityInTheCityLessThan2(struct PATH *path, char *city)
{
    struct PATH *now = path;
    int count = 0;
    while (now->next != NULL)
    {
        if (strcmp(now->city, city) == 0)
        {
            count++;
        }
        if (count >= 2)
        {
            break;
        }
        now = now->next;
    }
    if (count >= 2)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}
void findPath(char *startCity, char *endCity, int cargoWeight)
{
    //取得起始城市編號
    int startCityNo = 0;
    int i;
    struct PATH **pathes = malloc(sizeof(struct PATH *));
    int pathSize = 1;
    int pathSearchIndex = 0;
    int pathLoadIndex = 0;

    for (i = 0; i < cityNumber; i++)
    {
        if (strcmp(cities[i].name, startCity) == 0)
        {
            startCityNo = i;
            break;
        }
    }
    //printf("startCityNo %d",startCityNo);
    //把起始程式能夠到達的城市建立路徑
    for (i = 0; i < cityNumber; i++)
    {
        if (tunnelMap[startCityNo][i] == 1 && cities[i].throughput >= cargoWeight)
        {
            //printf("protencial route %s %s\n", cities[startCityNo].name, cities[i].name);
            struct PATH *path1 = getNewPath();
            path1 = addPathPoint(path1, cities[startCityNo].name);
            path1 = addPathPoint(path1, cities[i].name);
            //printf("new path %s %s\n",path1->city,path1->next->city);
            if (pathLoadIndex == pathSize - 1)
            {
                pathSize *= 2;
                pathes = realloc(pathes, sizeof(struct PATH) * pathSize);
            }
            pathes[pathLoadIndex] = path1;
            pathLoadIndex++;
        }
    }
    // printf("there %d route stored\n", pathLoadIndex);
    // for (i = 0; i < pathLoadIndex; i++)
    // {
    //     printf("protencial route %s \n", getPathOUTPUT( pathes[i]));

    // }
    // printf("\n");

    // printf("%s\n", getPathOUTPUT(pathes[0]));
    // printf("end of path1 %s\n",getEndOfPath(pathes[0])->city);
    // printf("%d\n",getCityNo(getEndOfPath(pathes[0])->city));

    struct OUTPUT *result = malloc(sizeof(struct OUTPUT));
    result->data = "";
    result->next = NULL;

    while (pathSearchIndex != pathLoadIndex)
    {

        //取出路徑，找到最後的城市，
        int endCityNo = getCityNo(getEndOfPath(pathes[pathSearchIndex])->city);
        //驗證最後城市所能相連的點是否已經走過
        for (i = 0; i < cityNumber; i++)
        {
            if (tunnelMap[endCityNo][i] == 1) //表示兩城市有聯通
            {
                if (cities[i].throughput >= cargoWeight) //表示可以通過
                {
                    if (!isCityInTheCity(pathes[pathSearchIndex], cities[i].name)) //如果沒有走過 記錄下來之後取出
                    {

                        // printf("%s , new city %s\n", getPathOUTPUT(pathes[pathSearchIndex]), cities[i].name);

                        struct PATH *newPath = addPathPoint(pathes[pathSearchIndex], cities[i].name);
                        if (strcmp(cities[i].name, endCity) == 0)
                        {
                            char *output = malloc(sizeof(char *));
                            sprintf(output, "%s", getPathOUTPUT(newPath));
                            result = addOUTPUT(result, output);
                        }
                        else
                        {
                            if (pathLoadIndex == pathSize - 1)
                            {
                                pathSize *= 2;
                                pathes = realloc(pathes, sizeof(struct PATH) * pathSize);
                            }
                            pathes[pathLoadIndex] = newPath;
                            pathLoadIndex++;
                        }
                    }
                }
            }
        }

        //直到沒有路徑可以取出
        pathSearchIndex++;
    }

    printOUTPUT(result);
}
int hasCircle(struct PATH *path)
{
    struct PATH *now = path;
    struct PATH *now2 = path;
    int count = 0;
    int count2 = 0;
    while (now != NULL)
    {
        //printf("test %s \n",now->city);
        count = 0;
        count2 = 0;
        now2 = path;
        while (now2 != NULL)
        {
            count2++;
            //printf("%s %s %d\n" ,now->city,now2->city,strcmp(now->city,now2->city));
            if (strcmp(now->city, now2->city) == 0)
            {
                count++;
            }
            if (count >= 2)
            {
                break;
            }
            now2 = now2->next;
        }
        if (count >= 2)
        {
            return count2;
        }
        now = now->next;
    }
    return 0;
}
void findPath2(char *startCity, char *endCity, int cargoWeight)
{
    //取得起始城市編號
    int startCityNo = 0;
    int i;
    struct PATH **pathes = malloc(sizeof(struct PATH *));
    int pathSize = 1;
    int pathSearchIndex = 0;
    int pathLoadIndex = 0;

    for (i = 0; i < cityNumber; i++)
    {
        if (strcmp(cities[i].name, startCity) == 0)
        {
            startCityNo = i;
            break;
        }
    }
    //printf("startCityNo %d",startCityNo);
    //把起始程式能夠到達的城市建立路徑
    for (i = 0; i < cityNumber; i++)
    {
        if (tunnelMap[startCityNo][i] == 1 && cities[i].throughput >= cargoWeight)
        {
            //printf("protencial route %s %s\n", cities[startCityNo].name, cities[i].name);
            struct PATH *path1 = getNewPath();
            path1 = addPathPoint(path1, cities[startCityNo].name);
            path1 = addPathPoint(path1, cities[i].name);
            //printf("new path %s %s\n",path1->city,path1->next->city);
            if (pathLoadIndex == pathSize - 1)
            {
                pathSize *= 2;
                pathes = realloc(pathes, sizeof(struct PATH) * pathSize);
            }
            pathes[pathLoadIndex] = path1;
            pathLoadIndex++;
        }
    }

    struct OUTPUT *result = malloc(sizeof(struct OUTPUT));
    result->data = "";
    result->next = NULL;

    while (pathSearchIndex < pathLoadIndex)
    {

        //取出路徑，找到最後的城市，
        int endCityNo = getCityNo(getEndOfPath(pathes[pathSearchIndex])->city);
        if (endCityNo == getCityNo(endCity))
        {
            pathSearchIndex++;
            continue;
        }
        //驗證最後城市所能相連的點是否已經走過
        for (i = 0; i < cityNumber; i++)
        {

            if (tunnelMap[endCityNo][i] == 1) //表示兩城市有聯通
            {
                if (cities[i].throughput >= cargoWeight) //表示可以通過
                {
                    if (isCityInTheCityLessThan2(pathes[pathSearchIndex], cities[i].name)) //如果沒有走過 記錄下來之後取出
                    {

                        // printf("%s , new city %s\n", getPathOUTPUT(pathes[pathSearchIndex]), cities[i].name);

                        struct PATH *newPath = addPathPoint(pathes[pathSearchIndex], cities[i].name);
                        if (strcmp(cities[i].name, endCity) == 0)
                        {
                            // char *output = malloc(sizeof(char *));
                            // sprintf(output, "%s", getPathOUTPUT(newPath));
                            // result = addOUTPUT(result, output);

                            if (pathLoadIndex == pathSize - 1)
                            {
                                pathSize *= 2;
                                pathes = realloc(pathes, sizeof(struct PATH) * pathSize);
                            }
                            pathes[pathLoadIndex] = newPath;
                            pathLoadIndex++;
                        }
                        else
                        {
                            if (pathLoadIndex == pathSize - 1)
                            {
                                pathSize *= 2;
                                pathes = realloc(pathes, sizeof(struct PATH) * pathSize);
                            }
                            pathes[pathLoadIndex] = newPath;
                            pathLoadIndex++;
                        }
                    }
                }
            }
        }
        //直到沒有路徑可以取出
        pathSearchIndex++;
        //printf("%d ,%d\n",pathSearchIndex,pathLoadIndex);
    }
    //printf("%d ,%d, %d\n", pathSearchIndex, pathLoadIndex, pathSize);
    // for (i = 0; i < pathLoadIndex; i++)
    // {
    //     struct PATH *now = pathes[i]->next;
    //     while (now != NULL)
    //     {
    //         printf("%s", now->city);
    //         if (now->next != NULL)
    //         {
    //             printf(",");
    //         }
    //         now = now->next;
    //     }
    //     printf("\n");
    // }

    for (int i = 0; i < pathLoadIndex; i++)
    {
        if (strcmp(getEndOfPath(pathes[i])->city, endCity) == 0)
        {
            int circleEnd = hasCircle(pathes[i]);
        
            //printf("circleEnd %d\n",circleEnd);
            if (circleEnd != 0)
            {
                struct PATH *now = pathes[i]->next;
                char *output = "";
                circleEnd--;
                for (int i = 0; i < circleEnd; i++)
                {
                    char *copy1 = malloc(sizeof(char *));
                    char *copy2 = malloc(sizeof(char *));
                    (copy1 = output);
                    sprintf(copy2, "%s%s", copy1, now->city);
                    (output = copy2);
                    if (i != circleEnd - 1)
                    {
                        (copy1 = output);
                        sprintf(copy2, "%s,", copy1);
                        (output = copy2);
                    }
                    now = now->next;
                }
                

                result = addOUTPUT(result, output);
            }
        }
    }
    //消除一樣的結果
    struct OUTPUT *now = result;
    while(now != NULL){
        if(now -> next == NULL){
            break;
        }
        if(strcmp(now->data,now->next->data) == 0){
            now->next = now->next->next;
        }
        now = now->next;
    }


    printOUTPUT(result);
}

void answer()
{
    //第一題

    // for (int i = 0; i < cityNumber; i++)
    // {
    //     printf("city name:%s , city throughput:%d\n", cities[i].name, cities[i].throughput);
    // }
    printf("Map: \n");
    struct OUTPUT *start = malloc(sizeof(struct OUTPUT));
    start->data = "";
    start->next = NULL;
    for (int i = 0; i < cityNumber; i++)
    {
        for (int j = 0; j < cityNumber; j++)
        {
            if (tunnelMap[i][j] == 1)
            {
                char *output = malloc(sizeof(char *));
                sprintf(output, "%s, %s", cities[i].name, cities[j].name);
                start = addOUTPUT(start, output);
            }
        }
    }
    printOUTPUT(start);
    //第二題
    printf("all the path from city %s to city %s\n", startCity, endCity);
    findPath(startCity, endCity, 0);
    //第三題
    printf("all the best path from city %s to city %s\n", startCity, endCity);
    findPath(startCity, endCity, atoi(cargoWeight));
    //第四題
    printf("all the path has circle form city %s to city %s\n", startCity, endCity);
    findPath2(startCity, endCity, 0);
}

int addCity(char *name, int throughput) //如果存在就回傳編號 不存在就新增一個城市 再回傳編號
{

    // cityNumber ++;
    // printf("%d \n",cityNumber);
    //printf("city: %s,throughput: %d\n", name, throughput);

    //確認是否已經存在
    int cityNo = -1;
    for (int i = 0; i < cityNumber; i++)
    {
        if (strcmp(cities[i].name, name) == 0)
        {
            cityNo = i;
            break;
        }
    }
    //printf("cityNo %d \n", cityNo);
    if (cityNo != -1)
    {
        //找到了
        //printf("exists \n");
    }
    else
    {
        //printf("not exists \n");
        //printf("%d \n",cityNumber);

        struct City c = {.name = name, .throughput = throughput};
        cities[cityNumber] = c;
        cityNo = cityNumber;
        cityNumber = cityNumber + 1;
    }
    return cityNo;
}

void initGame()
{
    cityNumber = 0;

    cities = calloc(MAX_CITY_NUM, sizeof(struct City));
    tunnelMap = calloc(MAX_CITY_NUM, sizeof(int *));
    for (int i = 0; i < MAX_CITY_NUM; i++)
    {
        tunnelMap[i] = calloc(MAX_CITY_NUM, sizeof(int));
    }
}

int main(int argc, char *argv[])
{

    int runningFlag = 1;
    int runningFlag2 = 1;

    FILE *fp;
    if ((fp = fopen("input.txt", "r")) == NULL)
    {
        printf("open file error\n");
    }
    char *input = calloc(10000, sizeof(char));
    char *seperator;
    char *dupstr;

    //char **cityNumber = calloc(MAX_CITY_NUM, sizeof(char *));
    while (runningFlag)
    {
        initGame();

        fscanf(fp, "%s", input);
        // printf("%s\n", input);
        seperator = ",";
        dupstr = strdup(input);
        startCity = strsep(&dupstr, seperator);
        endCity = strsep(&dupstr, seperator);
        cargoWeight = strsep(&dupstr, seperator);
        // printf("%s\n", startCity);
        // printf("%s\n", endCity);
        // printf("%s\n", cargoWeight);
        //第三題所需資料

        while (runningFlag2)
        {

            fscanf(fp, "%s", input);
            //printf("%s\n", input);
            dupstr = strdup(input);
            if (strcmp(dupstr, "0") == 0)
            {
                //printf("running continue\n");
                answer();
                runningFlag2 = 0; //這一回合結束
            }
            else if (strcmp(dupstr, "-1") == 0)
            {
                //printf("running\n");
                answer();
                runningFlag = 0; //沒有下一回合
                runningFlag2 = 0;
            }
            else
            {
                seperator = "->";
                char *data1 = strsep(&dupstr, seperator);
                int lastCityNum = -1;
                int nextCityNum = -1;
                while (data1 != NULL)
                {
                    //printf("%s\n", data1);

                    if (data1[0] != 0)
                    {
                        char *data2 = strsep(&data1, ",");
                        char *data3 = strsep(&data1, ",");
                        nextCityNum = addCity(data2, atoi(data3));
                        if (lastCityNum != -1)
                        {
                            tunnelMap[lastCityNum][nextCityNum] = 1; //相通
                        }
                        lastCityNum = nextCityNum;
                        //printf("city: %s,throughput: %d\n", data2, atoi( data3));
                    }

                    data1 = strsep(&dupstr, seperator);
                }
            }
        }

        runningFlag2 = 1; //復歸 開始跑下一輪
        printf("\n");
    }
    //printf("terminating\n");
    return 0;
}
